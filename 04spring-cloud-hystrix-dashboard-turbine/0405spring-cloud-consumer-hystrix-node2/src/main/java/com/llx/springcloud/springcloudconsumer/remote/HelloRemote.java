package com.llx.springcloud.springcloudconsumer.remote;

import com.llx.springcloud.springcloudconsumer.HelloRemoteHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="spring-cloud-producer2" , fallback = HelloRemoteHystrix.class)
public interface HelloRemote {

    @RequestMapping(value = "/hello")
    String hello2(@RequestParam(value = "name") String name);
}
