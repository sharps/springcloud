package com.llx.springcloud.springcloudconsumer;

import com.llx.springcloud.springcloudconsumer.remote.HelloRemote;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class HelloRemoteHystrix implements HelloRemote{

    public String hello(@RequestParam(value="name") String name){
        return "hello" +name+",this message send failed!";
    }

}
