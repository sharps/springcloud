##**注册中心Eureka**
###背景介绍
####服务中心
服务中心又称注册中心，管理各种服务功能包括服务的注册、发现、熔断、负载、降级等，比如dubbo admin后台的各种功能。<br/>
有了服务中心调用关系会有什么变化，画几个简图来帮忙理解<br/>
项目A调用项目B<br/>
正常调用项目A请求项目B<br/>

--------
```mermaid
graph LR
A[项目A] -- 请求 --> B[项目B] 
```
有了服务中心之后，任何一个服务都不能直接去掉用，都需要通过服务中心来调用

--------
```mermaid
graph LR
A[项目A] --> C(服<br/>务<br/>中<br/>心) 
C--> B[项目B] 
```
这时候调用的步骤就会为两步：第一步，项目A首先从服务中心请求项目B服务器，然后项目B在从服务中心请求项目C服务。

--------
```mermaid
graph LR
A[项目A] --> D(服<br/><br/>务<br/><br/>中<br/><br/>心) 
D--> C[项目B] 
B[项目B] -->D
D-->E[项目C] 
```

上面的项目只是两三个相互之间的简单调用，但是如果项目超过20个30个呢，画一张图来描述几十个项目之间的相互调用关系全是线条，任何其中的一个项目改动，就会牵连好几个项目跟着重启，巨麻烦而且容易出错。通过服务中心来获取服务你不需要关注你调用的项目IP地址，由几台服务器组成，每次直接去服务中心获取可以使用的服务去调用既可。<br/>
由于各种服务都注册到了服务中心，就有了去做很多高级功能条件。比如几台服务提供相同服务来做均衡负载；监控服务器调用成功率来做熔断，移除服务列表中的故障点；监控服务调用时间来对不同的服务器设置不同的权重等等。

####Eureka
按照官方介绍：
>Eureka is a REST (Representational State Transfer) based service that is primarily used in the AWS cloud for locating services for the purpose of load balancing and failover of middle-tier servers.

>Eureka 是一个基于 REST 的服务，主要在 AWS 云中使用, 定位服务来进行中间层服务器的负载均衡和故障转移。

Spring Cloud 封装了 Netflix 公司开发的 Eureka 模块来实现服务注册和发现。Eureka 采用了 C-S 的设计架构。Eureka Server 作为服务注册功能的服务器，它是服务注册中心。而系统中的其他微服务，使用 Eureka 的客户端连接到 Eureka Server，并维持心跳连接。这样系统的维护人员就可以通过 Eureka Server 来监控系统中各个微服务是否正常运行。Spring Cloud 的一些其他模块（比如Zuul）就可以通过 Eureka Server 来发现系统中的其他微服务，并执行相关的逻辑。

Eureka由两个组件组成：Eureka服务器和Eureka客户端。Eureka服务器用作服务注册服务器。Eureka客户端是一个java客户端，用来简化与服务器的交互、作为轮询负载均衡器，并提供服务的故障切换支持。Netflix在其生产环境中使用的是另外的客户端，它提供基于流量、资源利用率以及出错状态的加权负载均衡。

用一张图来认识以下：

--------
```mermaid66
graph LR
C[Service<br/>Provider] --Register/Renew/Cancel-->B[Eureka<br/>Server] 
A[Service<br/>Consumer] --Get Registry--> B[Eureka<br/>Server] 
A[Service<br/>Consumer] --Remote Call--> C[Service<br/>Provider] 
```

上图简要描述了Eureka的基本架构，由3个角色组成：<br/>
1、Eureka Server<br/>
提供服务注册和发现<br/>
2、Service Provider<br/>
服务提供方<br/>
将自身服务注册到Eureka，从而使服务消费方能够找到<br/>
3、Service Consumer<br/>
服务消费方<br/>
从Eureka获取注册服务列表，从而能够消费服务