###集群

注册中心这么关键的服务，如果是单点话，遇到故障就是毁灭性的。在一个分布式系统中，服务注册中心是最重要的基础部分，理应随时处于可以提供服务的状态。为了维持其可用性，使用集群是很好的解决方案。Eureka通过互相注册的方式来实现高可用的部署，所以我们只需要将Eureke Server配置其他可用的serviceUrl就能实现高可用部署。

####双节点注册中心
首次我们尝试一下双节点的注册中心的搭建。

1、创建application-peer1.properties，作为peer1服务中心的配置，并将serviceUrl指向peer2
```properties
spring.application.name=spring-cloud-eureka
server.port=8000
eureka.instance.hostname=peer1
eureka.client.serviceUrl.defaultZone=http://peer2:8001/eureka/
```
2、创建application-peer2.properties，作为peer2服务中心的配置，并将serviceUrl指向peer1
```properties
spring.application.name=spring-cloud-eureka
server.port=8001
eureka.instance.hostname=peer2
eureka.client.serviceUrl.defaultZone=http://peer1:8000/eureka/
```
3、host转换

在hosts文件中加入如下配置
```jshelllanguage
127.0.0.1 peer1  
127.0.0.1 peer2  
```
4、打包启动

依次执行下面命令

打包
```
mvn clean package
```
\# 分别以peer1和peeer2 配置信息启动eureka
```
java -jar spring-cloud-eureka-0.0.1-SNAPSHOT.jar --spring.profiles.active=peer1
java -jar spring-cloud-eureka-0.0.1-SNAPSHOT.jar --spring.profiles.active=peer2
```
依次启动完成后，浏览器输入：http://localhost:8000/ <br/>
可以看出peer1的注册中心DS Replicas已经有了peer2的相关配置信息，并且出现在available-replicas中。我们手动停止peer2来观察，发现peer2就会移动到unavailable-replicas一栏中，表示peer2不可用。<br/>

到此双节点的配置已经完成。
