##服务提供与调用
02eureka-producer-consumer<BR/>
---->0201spring-cloud-eureka <eureka server 服务注册中心 单机 端口：8888><BR/>
---->0202spring-cloud-producer-two<服务提供者two 端口：9000><BR/>
---->0203spring-cloud-producer-one<服务提供者one 端口：9001><BR/>
---->0204spring-cloud-consumer-one<服务消费者one 端口：9091><BR/>
---->0204spring-cloud-consumer-one<服务消费者one 端口：9092><BR/>

上一篇文章(01spring-cloud-eureka)我们介绍了eureka服务注册中心的搭建，这篇文章介绍一下如何使用eureka服务注册中心，搭建一个简单的服务端注册服务，客户端去调用服务使用的案例。

案例中有三个角色：服务注册中心、服务提供者、服务消费者，其中服务注册中心就是我们上一篇的eureka单机版启动既可，流程是首先启动注册中心，服务提供者生产服务并注册到服务中心中，消费者从服务中心中获取服务并执行。


